apiVersion: v1
kind: Service
metadata:
  name: mogdb
  labels:
    app: mogdb
    app.kubernetes.io/name: mogdb
spec:
  ports:
    - name: mogdb
      port: 5432
  clusterIP: None
  selector:
    app: mogdb
---
apiVersion: v1
kind: Service
metadata:
  name: mogdb-read
  labels:
    app: mogdb
    app.kubernetes.io/name: mogdb
    readonly: "true"
spec:
  ports:
    - name: mogdb
      port: 5432
  selector:
    app: mogdb
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mogdb
spec:
  selector:
    matchLabels:
      app: mogdb
      app.kubernetes.io/name: mogdb
  serviceName: mogdb
  replicas: 2
  template:
    metadata:
      labels:
        app: mogdb
        app.kubernetes.io/name: mogdb
    spec:
      shareProcessNamespace: true
      containers:
        - name: mogdb
          image: swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:5.0.8
          command:
            - bash
            - "-c"
            - |
              set -ex
              MogDB_Role=
              REPL_CONN_INFO=

              cat >>/home/omm/.profile <<-EOF
              export OG_SUBNET="0.0.0.0/0"
              export PGHOST="/var/lib/mogdb/tmp"
              EOF
              [[ -d "$PGHOST" ]] || (mkdir -p $PGHOST && chown omm $PGHOST)

              hostname=`hostname`
              [[ "$hostname" =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              if [[ $ordinal -eq 0 ]];then
                MogDB_Role="primary"
              else
                MogDB_Role="standby"
                echo "$hostname $PodIP" |ncat --send-only mogdb-0.mogdb 6543
                remote_ip=`ping mogdb-0.mogdb -c 1 | sed '1{s/[^(]*(//;s/).*//;q}'`
                REPL_CONN_INFO="replconninfo${ordinal} = 'localhost=$PodIP localport=5432 localservice=5434 remotehost=$remote_ip remoteport=5432 remoteservice=5434'"
              fi

              [[ -n "$REPL_CONN_INFO" ]] && export REPL_CONN_INFO
              source /home/omm/.profile
              exec bash /entrypoint.sh -M "$MogDB_Role"
          env:
            - name: PGHOST
              value: /var/lib/mogdb/tmp
            - name: PodIP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
          ports:
            - name: mogdb
              containerPort: 5432
          volumeMounts:
            - name: data
              mountPath: /var/lib/mogdb
              subPath: mogdb
          resources:
            requests:
              cpu: 500m
              memory: 1Gi
          livenessProbe:
            exec:
              command:
                - sh
                - -c
                - su -l omm -c "gsql -dpostgres -c 'select 1'"
            initialDelaySeconds: 120
            periodSeconds: 10
            timeoutSeconds: 5
            failureThreshold: 12
          readinessProbe:
            exec:
              # Check we can execute queries over TCP (skip-networking is off).
              command:
                - sh
                - -c
                - su -l omm -c "gsql -dpostgres -c 'select 1'"
            initialDelaySeconds: 30
            periodSeconds: 2
            timeoutSeconds: 1
        - name: sidecar
          image: swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:5.0.8
          ports:
            - name: sidecar
              containerPort: 6543
          command:
            - bash
            - "-c"
            - |
              set -ex
              cat >>/home/omm/.profile <<-EOF
              export PGHOST="/var/lib/mogdb/tmp"
              EOF
              source /home/omm/.profile
              while true;do
                ncat -l 6543 >/tmp/remote.info
                read host_name remote_ip < /tmp/remote.info
                [[ "$host_name" =~ -([0-9]+)$ ]] || exit 1
                remote_ordinal=${BASH_REMATCH[1]}

                repl_conn_info="replconninfo${remote_ordinal} = 'localhost=$PodIP localport=5432 localservice=5434 remotehost=$remote_ip remoteport=5432 remoteservice=5434'"
                echo "$repl_conn_info" >> "${PGDATA}/postgresql.conf"
                su - omm -c "gs_ctl reload"
              done
          env:
            - name: PodIP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
            - name: PGDATA
              value: "/var/lib/mogdb/data"
            - name: PGHOST
              value: "/var/lib/mogdb/tmp"
          volumeMounts:
            - name: data
              mountPath: /var/lib/mogdb
              subPath: mogdb
          resources:
            requests:
              cpu: 500m
              memory: 1Gi
  volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "local"
        resources:
          requests:
            storage: 2Gi