# 快速参考
- **维护者**：
[Enmotech OpenSource Team](https://github.com/enmotech)
- **哪里可以获取帮助**：
[墨天轮-MogDB](https://www.modb.pro/MogDB)

# 支持的tags
- [`latest`]()
- [`5.0.5`]()
- [`5.0.4`]()
- [`5.0.3`]()
- [`5.0.2`]()
- [`5.0.1`]()
- [`5.0.0`]()
- [`3.1.0`]()
- [`3.0.6`]() 
- [`3.0.5`]() 
- [`3.0.4`]() 
- [`3.0.3`]() 
- [`3.0.2`]() 
- [`3.0.1`]() 
- [`3.0.0`]()  
- [`2.1.0`]()
- [`2.0.3`]()  
- [`2.0.1`]()
- [`2.0.0`]()
- [`1.1.0`]()
- [`1.0.1`]()
- [`1.0.0`]()


# 关于MogDB
MogDB是云和恩墨基于MogDB开源数据库进行定制、推出的企业发行版。它将围绕高可用、安全、自动化运维、数据库一体机和SQL审核优化等企业需求，解决企业用户落地。其核心价值是易用性、高性能、高可用等和全天候的企业支持。

MogDB官方网站：[https://mogdb.io/](https://mogdb.io/)

![logo](https://cdn-mogdb.enmotech.com/website/logo.png)


# 云和恩墨MogDB镜像的特点
* 云和恩墨会最紧密跟踪MogDB的源码变化，第一时间发布镜像的新版本。
* 云和恩墨的云端数据库，虚拟机数据库以及容器版本数据库均会使用同样的初始化最佳实践配置，这样当您在应对各种不同需求时会有几乎相同的体验。
* 云和恩墨会持续发布不同CPU架构（x86或者ARM）之上，不同操作系统的各种镜像  
* 云和恩墨MogDB 2.1容器版支持最新版的compat-tools和插件功能。


  * [compat-tools详细介绍](https://docs.mogdb.io/zh/mogdb/v2.1/DBMS-RANDOM#compat-tools%E4%BB%8B%E7%BB%8D)
  * 插件详细介绍
    * [dblink](https://docs.mogdb.io/zh/mogdb/v2.1/dblink-user-guide)  
    * [orafce](https://docs.mogdb.io/zh/mogdb/v2.1/orafce-user-guide)  
    * [pg_buikload](https://docs.mogdb.io/zh/mogdb/v2.1/pg_bulkload-user-guide)
    * [pg_prewarm](https://docs.mogdb.io/zh/mogdb/v2.1/pg_prewarm-user-guide)
    * [pg_repack](https://docs.mogdb.io/zh/mogdb/v2.1/pg_repack-user-guide)
    * [pg_trgm](https://docs.mogdb.io/zh/mogdb/v2.1/pg_trgm-user-guide)
    * [wal2json](https://docs.mogdb.io/zh/mogdb/v2.1/wal2json-user-guide)
    
**目前已经支持x86-64和ARM64两种架构。**

从2.0版本开始（包括2.0版本）
- x86-64架构的MogDB运行在[Ubuntu 18.04操作系统](https://ubuntu.com/)中
- ARM64架构的MogDB运行在[Debian 10 操作系统](https://www.debian.org/)中

在1.1.0版本之前（包括1.1.0版本）
- x86-64架构的MogDB运行在[CentOS 7.6操作系统](https://www.centos.org/)中
- ARM64架构的MogDB运行在[openEuler 20.03 LTS操作系统](https://openeuler.org/zh/)中

# 如何使用本镜像

## 启动MogDB实例

```console
$ docker run --name MogDB --privileged=true -d -e GS_PASSWORD=Enmo@123 swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:latest
```

## 环境变量
为了更灵活的使用MogDB镜像，可以设置额外的参数。未来我们会扩充更多的可控制参数，当前版本支持以下变量的设定。

### `GS_PASSWORD`
在你使用MogDB镜像的时候，必须设置该参数。该参数值不能为空或者不定义。该参数设置了MogDB数据库的超级用户omm以及测试用户gaussdb的密码。MogDB安装时默认会创建omm超级用户，该用户名暂时无法修改。测试用户gaussdb是在[entrypoint.sh](https://github.com/enmotech/enmotech-docker-MogDB/blob/master/1.0.1/entrypoint.sh)中自定义创建的用户。

MogDB镜像配置了本地信任机制，因此在容器内连接数据库无需密码，但是如果要从容器外部（其它主机或者其它容器）连接则必须要输入密码。

**MogDB的密码有复杂度要求，需要：密码长度8个字符及以上，必须同时包含英文字母大小写，数字，以及特殊符号**

### `GS_NODENAME`

指定数据库节点名称 默认为gaussdb

### `GS_USERNAME`

指定数据库连接用户名 默认为gaussdb

### `GS_PORT`
指定数据库端口，默认为5432。

## 从容器外部连接容器数据库
MogDB的默认监听启动在容器内的5432端口上，如果想要从容器外部访问数据库，则需要在`docker run`的时候指定`-p`参数。比如以下命令将允许使用15432端口访问容器数据库。
```console
$ docker run --name MogDB --privileged=true -d -e GS_PASSWORD=Secretpassword@123 -p 15432:5432 swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:latest
```
在上述命令正常启动容器数据库之后，可以通过外部的gsql进行数据库访问。
```console
$ gsql -d postgres -U mogdb -W'Secretpassword@123' -h your-host-ip -p15432
```


## 持久化存储数据
容器一旦被删除，容器内的所有数据和配置也均会丢失，而从镜像重新运行一个容器的话，则所有数据又都是呈现在初始化状态，因此对于数据库容器来说，为了防止因为容器的消亡或者损坏导致的数据丢失，需要进行持久化存储数据的操作。通过在`docker run`的时候指定`-v`参数来实现。比如以下命令将会指定将MogDB的所有数据文件存储在宿主机的/enmotech/mogdb下。`-u root`参数用于指定容器启动的时候以root用户执行脚本，否则会遇到没有权限创建数据文件目录的问题。

注：如果使用podman，会有目标路径检查，需要预先创建宿主机目标路径。

```console
$ mkdir -p /enmotech/mogdb
$ docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Secretpassword@123 \
    -v /enmotech/mogdb:/var/lib/mogdb  -u root -p 15432:5432 \
    swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:latest
```

## Kubernetes 部署 (2.1.0版本以后支持)

- x86
```console
$ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/2.1.0/k8s_amd.yaml
pod/mogdb created
```

- arm
```console
$ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/2.1.0/k8s_arm.yaml
pod/mogdb created
```



**k8s连接pod**

- 进入容器内gsql登录
```console
$ kubectl exec -it mogdb -- bash
root@mogdb:/# su - omm
omm@mogdb:~$ gsql -d postgres
gsql ((MogDB 2.1.0 build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```


## 创建主从复制的MogDB容器
1. 拉取容器镜像
2. 运行脚本[create_master_slave.sh](https://github.com/enmotech/enmotech-docker-MogDB/blob/master/create_master_slave.sh)，按照提示输入所需参数，或者直接使用默认值，即可自动创建MogDB一主一备架构的两个容器。

上述脚本有多个自定义参数，以下为这些参数的参数名称(解释)[默认值]。  
> OG_SUBNET (容器所在网段) [172.11.0.0/24]  
GS_PASSWORD (定义数据库密码)[Enmo@123]  
MASTER_IP (主库IP)[172.11.0.101]  
SLAVE_1_IP (备库IP)[172.11.0.102]  
MASTER_HOST_PORT (主库数据库服务端口)[5432]  
MASTER_LOCAL_PORT (主库通信端口)[5434]  
SLAVE_1_HOST_PORT (备库数据库服务端口)[6432]  
SLAVE_1_LOCAL_PORT (备库通信端口)[6434]  
MASTER_NODENAME (主节点名称)[MogDB_master]  
SLAVE_NODENAME （备节点名称）[MogDB_slave1]  

### 使用示例
#### 拉取镜像
```console
# docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:latest
```
#### 获取创建主备容器脚本并运行
```console
# wget https://raw.githubusercontent.com/enmotech/enmotech-docker-MogDB/master/create_master_slave.sh
# chmod +x create_master_slave.sh 
# ./create_master_slave.sh 
Please input OG_SUBNET (容器所在网段) [172.11.0.0/24]: 
OG_SUBNET set 172.11.0.0/24
Please input GS_PASSWORD (定义数据库密码)[Enmo@123]: 
GS_PASSWORD set Enmo@123
Please input MASTER_IP (主库IP)[172.11.0.101]: 
MASTER_IP set 172.11.0.101
Please input SLAVE_1_IP (备库IP)[172.11.0.102]: 
SLAVE_1_IP set 172.11.0.102
Please input MASTER_HOST_PORT (主库数据库服务端口)[5432]: 
MASTER_HOST_PORT set 5432
Please input MASTER_LOCAL_PORT (主库通信端口)[5434]: 
MASTER_LOCAL_PORT set 5434
Please input SLAVE_1_HOST_PORT (备库数据库服务端口)[6432]: 
SLAVE_1_HOST_PORT set 6432
Please input SLAVE_1_LOCAL_PORT (备库通信端口)[6434]: 
SLAVE_1_LOCAL_PORT set 6434
Please input MASTER_NODENAME [MogDB_master]: 
MASTER_NODENAME set MogDB_master
Please input SLAVE_NODENAME [MogDB_slave1]: 
SLAVE_NODENAME set MogDB_slave1
Please input MogDB VERSION [1.0.1]: 
MogDB VERSION set 1.0.1
starting  
a70b46c7b2ddd1b6959403a0ac5b6783cf3f4100404fa628b8f055352a3e8567
MogDB Database Network Created.
e5430f16948639ac6a681e7f7db5ebbce8bf40c576e17ae412a3003f27b8ea14
MogDB Database Master Docker Container created.
bcb688c551b15d34196c249fdf934e4b8140a9181d6dde809c957405ec1ed29a
MogDB Database Slave1 Docker Container created.
```

#### 验证主从状态
```
# docker exec -it MogDB_master /bin/bash
# su - omm
Last login: Thu Oct  1 23:19:49 UTC 2020 on pts/0
$ gs_ctl query -D /var/lib/mogdb/data/
[2020-10-01 23:21:27.685][316][][gs_ctl]: gs_ctl query ,datadir is -D "/var/lib/mogdb/data"  
 HA state:           
        local_role                     : Primary
        static_connections             : 1
        db_state                       : Normal
        detail_information             : Normal

 Senders info:       
        sender_pid                     : 258
        local_role                     : Primary
        peer_role                      : Standby
        peer_state                     : Normal
        state                          : Streaming
        sender_sent_location           : 0/3000550
        sender_write_location          : 0/3000550
        sender_flush_location          : 0/3000550
        sender_replay_location         : 0/3000550
        receiver_received_location     : 0/3000550
        receiver_write_location        : 0/3000550
        receiver_flush_location        : 0/3000550
        receiver_replay_location       : 0/3000550
        sync_percent                   : 100%
        sync_state                     : Sync
        sync_priority                  : 1
        sync_most_available            : On
        channel                        : 172.11.0.101:5434-->172.11.0.102:53786

 Receiver info:      
No information 
```

# 快速运行起MogDB主备集群

本页展示如何使用 StatefulSet 控制器运行一个MogDB 有状态的集群。示例应用的拓扑结构有一个主服务器和多个副本，使用异步的基于行（Row-Based） 的数据复制。

## 在你开始之前

- 你必须拥有一个 Kubernetes 的集群，同时你的 Kubernetes 集群必须带有 kubectl 命令行工具。 建议在至少有两个节点的集群上运行本教程，且这些节点不作为控制平面主机。 如果你还没有集群，你可以通过 Minikube 构建一个你自己的集群。
- 你需要有一个带有默认 StorageClass的 动态 PersistentVolume 供应程序，或者自己静态的提供 PersistentVolume 来满足这里使用的 PersistentVolumeClaim。
- 本教程假定你熟悉 PersistentVolumes 与 StatefulSet, 以及其他核心概念，例如 Pod、 服务 与 ConfigMap。
- 熟悉 MySQL 会有所帮助，但是本教程旨在介绍对其他系统应该有用的常规模式。
- 你正在使用默认命名空间或不包含任何冲突对象的另一个命名空间。

## 部署 MogDB 主备集群

此示例部署包含两个PersistentVolume、两个Service和一个StatefulSet。

### 创建两个 PersistentVolume

使用以下的 YAML 配置文件创建 ConfigMap：

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mogdb-pv0
  labels:
    app: mogdb
spec:
  capacity:
    storage: 20Gi
  accessModes: ["ReadWriteOnce"]
  persistentVolumeReclaimPolicy: Retain
  storageClassName: local
  local:
    path: /mogdb-data/0
  nodeAffinity:
    required:
      nodeSelectorTerms:
        - matchExpressions:
            - key: kubernetes.io/hostname
              operator: In
              values:
                - k8s-002
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mogdb-pv1
  labels:
    app: mogdb
spec:
  capacity:
    storage: 20Gi
  accessModes: ["ReadWriteOnce"]
  persistentVolumeReclaimPolicy: Retain
  storageClassName: local
  local:
    path: /mogdb-data/1
  nodeAffinity:
    required:
      nodeSelectorTerms:
        - matchExpressions:
            - key: kubernetes.io/hostname
              operator: In
              values:
                - k8s-002
```

```shell
kubectl apply -f mogdb-pv.yml
```

制作的静态 pv 卷

### 创建 Service

使用以下 YAML 配置文件创建服务：

```yaml
apiVersion: v1
kind: Service
metadata:
  name: mogdb
  labels:
    app: mogdb
    app.kubernetes.io/name: mogdb
spec:
  ports:
    - name: mogdb
      port: 5432
  clusterIP: None
  selector:
    app: mogdb
---
apiVersion: v1
kind: Service
metadata:
  name: mogdb-read
  labels:
    app: mogdb
    app.kubernetes.io/name: mogdb
    readonly: "true"
spec:
  ports:
    - name: mogdb
      port: 5432
  selector:
    app: mogdb
```

```shell
kubectl apply -f mogdb-services.yml
```

标识 clusterIP: None 的 Service 为 Headless Service，Headless Service 给 StatefulSet 控制器为集合中每个 Pod 创建的 DNS 条目提供了一个宿主。这样就可以通过同一 Kubernetes 集群和命名空间中的任何其他 Pod 内解析 < Pod 名称 >.mysql 来访问 Pod。所以客户端应直接连接到 MySQL 主节点 Pod （通过其在无头 Service 中的 DNS 条目）以执行写入操作。

名称为mogdb-read 的 Service 是一种 常规Servcie，具有自己的集群IP，该集群 IP 在所有准备就绪的 MogDB Pod 之间分配连接，提供负载均衡。通常用来执行读操作。

### 创建 StatefulSet

最后，使用以下 YAML 配置文件创建 StatefulSet：

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mogdb
spec:
  selector:
    matchLabels:
      app: mogdb
      app.kubernetes.io/name: mogdb
  serviceName: mogdb
  replicas: 2
  template:
    metadata:
      labels:
        app: mogdb
        app.kubernetes.io/name: mogdb
    spec:
      shareProcessNamespace: true
      containers:
        - name: mogdb
          image: swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.1.0
          command:
            - bash
            - "-c"
            - |
              set -ex
              MogDB_Role=
              REPL_CONN_INFO=
              
              cat >>/home/omm/.profile <<-EOF
              export OG_SUBNET="0.0.0.0/0"
              export PGHOST="/var/lib/mogdb/tmp"
              EOF
              [[ -d "$PGHOST" ]] || (mkdir -p $PGHOST && chown omm $PGHOST)

              hostname=`hostname`
              [[ "$hostname" =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              if [[ $ordinal -eq 0 ]];then
                MogDB_Role="primary" 
              else
                MogDB_Role="standby"
                echo "$hostname $PodIP" |ncat --send-only mogdb-0.mogdb 6543
                remote_ip=`ping mogdb-0.mogdb -c 1 | sed '1{s/[^(]*(//;s/).*//;q}'`
                REPL_CONN_INFO="replconninfo${ordinal} = 'localhost=$PodIP localport=5432 localservice=5434 remotehost=$remote_ip remoteport=5432 remoteservice=5434'"
              fi

              [[ -n "$REPL_CONN_INFO" ]] && export REPL_CONN_INFO
              source /home/omm/.profile
              exec bash /entrypoint.sh -M "$MogDB_Role"
          env:
            - name: PGHOST
              value: /var/lib/mogdb/tmp
            - name: PodIP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
          ports:
            - name: mogdb
              containerPort: 5432
          volumeMounts:
            - name: data
              mountPath: /var/lib/mogdb
              subPath: mogdb
          resources:
            requests:
              cpu: 500m
              memory: 1Gi
          livenessProbe:
            exec:
              command:
                - sh
                - -c
                - su -l omm -c "gsql -dpostgres -c 'select 1'"
            initialDelaySeconds: 120
            periodSeconds: 10
            timeoutSeconds: 5
            failureThreshold: 12
          readinessProbe:
            exec:
              # Check we can execute queries over TCP (skip-networking is off).
              command:
                - sh
                - -c
                - su -l omm -c "gsql -dpostgres -c 'select 1'"
            initialDelaySeconds: 180
            periodSeconds: 2
            timeoutSeconds: 1
        - name: sidecar
          image: swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.1.0
          ports:
            - name: sidecar
              containerPort: 6543
          command:
            - bash
            - "-c"
            - |
              set -ex      
              cat >>/home/omm/.profile <<-EOF
              export PGHOST="/var/lib/mogdb/tmp"
              EOF
              source /home/omm/.profile
              while true;do
                ncat -l 6543 >/tmp/remote.info
                read host_name remote_ip < /tmp/remote.info
                [[ "$host_name" =~ -([0-9]+)$ ]] || exit 1
                remote_ordinal=${BASH_REMATCH[1]}
              
                repl_conn_info="replconninfo${remote_ordinal} = 'localhost=$PodIP localport=5432 localservice=5434 remotehost=$remote_ip remoteport=5432 remoteservice=5434'"
                echo "$repl_conn_info" >> "${PGDATA}/postgresql.conf"
                su - omm -c "gs_ctl reload"
              done
          env:
            - name: PodIP
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: status.podIP
            - name: PGDATA
              value: "/var/lib/mogdb/data"
            - name: PGHOST
              value: "/var/lib/mogdb/tmp"
          volumeMounts:
            - name: data
              mountPath: /var/lib/mogdb
              subPath: mogdb
          resources:
            requests:
              cpu: 500m
              memory: 1Gi
  volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "local"
        resources:
          requests:
            storage: 10Gi
```

```shell
kubectl apply -f mogdb-statefulset.yml
```

你可以通过运行以下命令查看启动进度：

```shell
kubectl get pods -l app=mgodb --watch
```

一段时间后，你应该看到所有 2 个 Pod 进入 Running 状态：

```text
NAME      READY     STATUS    RESTARTS   AGE
mysql-0   2/2       Running   0          2m
mysql-1   2/2       Running   0          1m
```

输入 **Ctrl+C** 结束监视操作。


# License
Copyright (c) 2011-2021 Enmotech

许可证协议遵循GPL v3.0，你可以从下方获取协议的详细内容。

    https://gitee.com/enmotech/enmotech-docker-mogdb/blob/master/LICENSE
